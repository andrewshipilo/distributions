#ifndef DISTRIBUTIONS_DISTRIBUTION_H
#define DISTRIBUTIONS_DISTRIBUTION_H


#include "RandomGenerator.h"
#include "MacLarenMarsagliaGenerator.h"

class IDistribution
{
public:
    virtual double operator()(RandomGenerator* generator) = 0;
};


#endif
