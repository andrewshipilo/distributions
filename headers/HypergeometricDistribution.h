#ifndef DISTRIBUTIONS_HYPERGEOMETRICDISTRIBUTION_H
#define DISTRIBUTIONS_HYPERGEOMETRICDISTRIBUTION_H

#include <cmath>
#include <boost/math/distributions/hypergeometric.hpp>
#include "IDistribution.h"
using namespace boost::math;




class HypergeometricDistribution : public IDistribution
{
public:
    explicit HypergeometricDistribution(double r, double n, double N)
    : r(r), n(n), N(N) {}

    double operator()(RandomGenerator *generator) override
    {
        double x = 0;

        x = combination(x, N) * combination(r - x, n - N) / combination(r, n);

        return x;
    }

    void draw()
    {
        std::cout << "Hypergeometric" << std::endl;
        hypergeometric_distribution<double> hg_dist(r, n, N);
        std::cout.precision(4);

        for (int k = 0; k < r; k++)
        {
            if (k + 1 == 10)
                std::cout << " ";
            else if (k < 10)
                std::cout << "  ";

            std::cout << k << "-" << (k + 1) << ":";
            auto num = boost::math::pdf<double>(hg_dist, k);
            std::cout << std::string(num * 195, '*') << std::endl;
        }
    }

private:
    double combination(double x, double y)
    {
        return std::tgamma(x + 1) / std::tgamma(y + 1) * std::tgamma( (x + 1) - (y + 1));
    }

    double r;
    double n;
    double N;
};


#endif
