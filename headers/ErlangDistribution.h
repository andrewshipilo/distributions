#ifndef DISTRIBUTIONS_ERLANGDISTRIBUTION_H
#define DISTRIBUTIONS_ERLANGDISTRIBUTION_H

#include "IDistribution.h"

class ErlangDistribution : public IDistribution
{
public:
    explicit ErlangDistribution(int K, double lambda) : lambda(lambda), K(K) {}

    double operator()(RandomGenerator* generator) override
    {
        double x = 0;

        for (int i = 0; i < K; i++)
        {
            x += std::log(generator->next());
        }

        x *= - (1 / lambda);

        return x;
    }

private:
    double lambda;
    int    K;
};

#endif
