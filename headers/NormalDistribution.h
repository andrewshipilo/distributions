#ifndef DISTRIBUTIONS_NORMALDISTRIBUTION_H
#define DISTRIBUTIONS_NORMALDISTRIBUTION_H


#include "IDistribution.h"

class NormalDistribution: public IDistribution
{
public:
    explicit NormalDistribution(double mu, double sigma) : sigma(sigma), mu(mu) {}

    double operator()(RandomGenerator* generator) override
    {
        double x = 0;

        x = sqrt(-2 * log(generator->next())) * std::cos(2 * M_PI * generator->next());

        return x * sigma + mu;
    }

private:
    double sigma;
    double mu;
};


#endif
