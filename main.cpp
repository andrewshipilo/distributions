#include <iostream>
#include <random>
#include "headers/IDistribution.h"
#include "headers/ExponentialDistribution.h"
#include "headers/UniformDistribution.h"
#include "headers/ErlangDistribution.h"
#include "headers/NormalDistribution.h"
#include "headers/HypergeometricDistribution.h"
#include <boost/math/distributions/hypergeometric.hpp>

const int nrolls = 100000; // number of experiments
const int nstars = 195;   // maximum number of stars to distribute
const int nintervals = 10;

void draw(const std::string &name, std::uniform_real_distribution<double> &dist)
{
    std::default_random_engine generator;

    int p[nintervals] = {};

    for (int i = 0; i < nrolls; ++i)
    {
        double number = dist(generator);
        if (number < 1.0)
            ++p[int(nintervals * number)];
    }

    std::cout << name << std::endl;
    for (int i = 0; i < 10; ++i)
        std::cout << i << ": " << std::string(p[i] * nstars / nrolls, '*') << std::endl;
}

void draw(const std::string &name, IDistribution &dist)
{
    auto mmg = new MacLarenMarsagliaGenerator(20 * nrolls);

    int p[nintervals] = {};

    for (int i = 0; i < nrolls; ++i)
    {
        double number = dist(mmg);
        if (number < 1.0)
            ++p[int(nintervals * number)];
    }

    std::cout << name << std::endl;
    std::cout << std::fixed;
    std::cout.precision(1);

    for (int i = 0; i < nintervals; ++i)
    {
        std::cout << float(i) / nintervals << "-" << float(i + 1) / nintervals << ": ";
        std::cout << std::string(p[i] * nstars / nrolls, '*') << std::endl;
    }
    std::cout << std::endl;

    delete mmg;
}

void drawErlang(const std::string &name, IDistribution &dist)
{
    auto mmg = new MacLarenMarsagliaGenerator(20 * nrolls);

    int p[nintervals] = {};

    for (int i = 0; i < nrolls; ++i)
    {
        double number = dist(mmg);
        if ((number >= 0.0) && (number < 10.0))
            ++p[int(number)];
    }

    std::cout << name << std::endl;
    std::cout << std::fixed;
    std::cout.precision(1);

    for (int i = 0; i < nintervals; ++i)
    {
        std::cout << i << "-" << (i+1) << ": ";
        std::cout << std::string(p[i] * nstars / nrolls, '*') << std::endl;
    }
    std::cout << std::endl;

    delete mmg;
}

int main()
{
    ExponentialDistribution expDist(3.5);
    std::exponential_distribution<double> dist(3.5);

    //draw("Exponential default", dist);
    draw("Exponential", expDist);

    UniformDistribution uniDist(0.0, 1.0);
    std::uniform_real_distribution<double> unidist(0.0, 1.0);

    //draw("Uniform default", unidist);
    draw("Uniform", uniDist);

    ErlangDistribution erlDist(5, 1.0);
    drawErlang("Erlang", erlDist);

    NormalDistribution nrmDist(5.0, 2.0);
    drawErlang("Normal", nrmDist);

    HypergeometricDistribution hprDist(50, 100, 500);
    hprDist.draw();

    return 0;
}