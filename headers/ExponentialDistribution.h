#ifndef DISTRIBUTIONS_EXPONENTIONALDISTRIBUTION_H
#define DISTRIBUTIONS_EXPONENTIONALDISTRIBUTION_H


#include "IDistribution.h"

class ExponentialDistribution : public IDistribution
{
public:
    explicit ExponentialDistribution(double lambda) : lambda(lambda){}

    double operator()(RandomGenerator* generator) override
    {
        double x = 0;

        x = - (1 / lambda) * std::log(generator->next());

        return x;
    }

private:
    double lambda;
};


#endif
