#ifndef DISTRIBUTIONS_UNIFORMDISTRIBUTION_H
#define DISTRIBUTIONS_UNIFORMDISTRIBUTION_H
#include "IDistribution.h"

class UniformDistribution : public IDistribution
{
public:
    explicit UniformDistribution(double A, double B) : A(A), B(B){}

    double operator()(RandomGenerator* generator) override
    {
        double x = 0;

        x = A + (B - A) * generator->next();

        return x;
    }

private:
    double A;
    double B;
};


#endif
